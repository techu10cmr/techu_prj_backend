const debug = require ('debug');

const init = debug ('app:init');
const middleware = debug ('app:middleware');
const validator = debug ('app:validator');
const controller = debug ('app:controller');
const net = debug ('app:net');
const api = debug ('app:api');


/********** Exporta para uso por terceros **********/
module.exports = {
  init, middleware, validator,  controller , net, api
}; 
