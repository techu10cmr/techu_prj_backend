/******************  Carga módulos terceros ******************/
const bcrypt = require('bcrypt');

/**
 * Genera el hash de los datos pasados como parámetro
 * Ojo! Llamada sincrona
 * 
 * @param  {} data datos al que generar el hash
 * 
 * @returns hash de los datos pasados como parámetro con salt=10
 */
function hash(data) {
  return bcrypt.hashSync(data, 10);
}


/**
 * Compara los datos con un hash 
 * Ojo! Llamada sincrona
 * 
 * @param  {} data datos a comparar
 * @param  {} hash hash a comparar
 * 
 * @returns true si son iguales y false en caso contrario
 */
function compare(data, hash) {
  return bcrypt.compareSync(data, hash);
}


/********** Exporta funciones para uso por terceros **********/
module.exports.hash = hash;
module.exports.compare = compare;