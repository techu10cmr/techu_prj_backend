/******************  Carga módulos terceros ******************/


const simpleUrl = require('simple-url');
const debugNet = require('../utils/debuggers').net;
const errors = require('request-promise-native/errors');

function composeBasicRequest(format, sMethod, objHeaders, uri, objQueryString, objBody, funcTransform, onlyTransformInOK) {

  var request = {};
  if (String(format).toUpperCase() === 'JSON') {
    request.json = true;
  }
  if (sMethod) {
    request.method = String(sMethod);
  }
  if (uri) {
    request.uri = uri;
  }
  if (objHeaders) {
    request.headers = objHeaders;
  }
  request.useQuerystring = true;
  if (objQueryString) {
    request.qs = objQueryString;
  }
  if (objBody) {
    request.body = objBody;
  }
  if (funcTransform) {
    request.transform = funcTransform;
  }
  if (onlyTransformInOK) {
    request.transform2xxOnly = true;
  }


  return (request);
}



function composeUrl(argProtocol, argHost, argPath) {
  var newUrl = {
    protocol: argProtocol,
    host: argHost,
    pathname: argPath,
  };
  return (simpleUrl.create(newUrl));
}

function sendValidationErrorResponse(req, res, body, statusCode) {
  debugNet("Executing sendValidationErrorResponse");
  res.status(statusCode).send(body);
}

function sendOKResponse(req, res, body, statusCode) {
  debugNet("Executing sendOKResponse");
  res.status(statusCode).send(body);
}

function sendOKResponseWithHeaders(req, res, headers, body, statusCode) {
  debugNet("Executing sendOKResponseWithHeaders");

  if (headers) {
    headers.forEach(header => {
      res.setHeader(header.name, header.value);
    });
  }

  res.status(statusCode).send(body);
}

function sendErrorResponse(req, res, error) {
  debugNet("Executing sendErrorResponse");
  res.status(error.code).send({
    "msg": `${error.msg}`,
    "errors": `${error.errors}`
  });
}

function composeErrorResponse(code, msg, errors) {
  debugNet("Executing composeErrorResponse");
  return {
    "code": code,
    "msg": msg,
    "errors": errors
  };
}

function composeOKResponse(code, msg) {
  debugNet("Executing composeOKResponse");
  return {
    "code": code,
    "msg": msg
  };
}




/********** Exporta funciones para uso por terceros **********/
module.exports.composeUrl = composeUrl;
module.exports.composeBasicRequest = composeBasicRequest;
module.exports.sendValidationErrorResponse = sendValidationErrorResponse;
module.exports.sendOKResponse = sendOKResponse;
module.exports.sendOKResponseWithHeaders = sendOKResponseWithHeaders;
module.exports.sendErrorResponse = sendErrorResponse;
module.exports.composeErrorResponse = composeErrorResponse;
module.exports.composeOKResponse = composeOKResponse;
