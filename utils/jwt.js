var fs = require('fs');

var jwt = require('jsonwebtoken');

exports.createToken = function (user) {
  var payload = {
    data: eval(process.env.JWT_DATA_FIELD)
    
/*    ,
    audience: `${process.env.JWT_AUDIENCE}`,
    expiresIn: `${process.env.JWT_EXPIRATION}`,
    algorithm: `${process.env.JWT_ENCODER_ALGORITHM}`
*/    
  };

  var privateKEY = fs.readFileSync('keys/private.key', 'utf8');

  return jwt.sign(payload, privateKEY);
};

exports.verifyToken = function (token) {

     
  var verifyOptions = {
    data: eval(process.env.JWT_DATA_FIELD)
     /*
    issuer: `${process.env.JWT_ISSUER}`,
    subject: eval(process.env.JWT_SUBJECT),
    audience: `${process.env.JWT_AUDIENCE}`,
    expiresIn: `${process.env.JWT_EXPIRATION}`,

    algorithm: [`${process.env.JWT_ENCODER_ALGORITHM}`]
        */

  };
    
  
  var publicKEY = fs.readFileSync('keys/public.key', 'utf8');

  try {
    return jwt.verify(token, publicKEY,verifyOptions);
  } catch (err) {
    return false;
  }
};