function getRequestValidator(argOptions) {
    var RequestValidator = require('ajv-request-validator');
    var defaultOptions = {
        allErrors: true,
        jsonPointers: true
    };

    var options = (argOptions) ? Object.assign(defaultOptions, argOptions) : defaultOptions;
    var reqValidator = new RequestValidator(options);

    require('ajv-errors')(reqValidator.ajv);
  
    return (reqValidator);

}


function compileValidatorRequestJSONSchema(reqValidator, pathSchema) {
    if (reqValidator && pathSchema) {
        return (reqValidator.compile(require(pathSchema)));
    }
}



function validateDNI(dni) {
    var lookup = 'TRWAGMYFPDXBNJZSQVHLCKE';
    var valueDni = dni.substr(0, dni.length - 1);
    var letter = dni.substr(dni.length - 1, 1).toUpperCase();

    return (lookup.charAt(valueDni % 23) == letter);
}
/********** Exporta funciones para uso por terceros **********/
module.exports.getRequestValidator = getRequestValidator;
module.exports.compileValidatorRequestJSONSchema = compileValidatorRequestJSONSchema;
module.exports.validateDNI = validateDNI;