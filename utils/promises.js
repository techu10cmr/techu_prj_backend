exports.createPromise = (funcBoolDecision, funcDataForResolve, funcDataForReject) => new Promise(
    (resolve, reject) => funcBoolDecision ? resolve(funcDataForResolve.call(this,funcBoolDecision)) : reject(funcDataForReject.call(this,funcBoolDecision))
);