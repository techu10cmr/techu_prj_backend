exports.removeDuplicatesArray = (array) => (array.filter((object, index) => index === array.findIndex(obj => JSON.stringify(obj) === JSON.stringify(object))));

exports.joinAndStringifyPairsKeyValueArray = (array) => (array.map((searchObj) => {
        var {
            nameField,
            valueField
        } = searchObj;
        return (typeof valueField === 'string' || valueField instanceof String) ? `"${nameField}":"${valueField}"` : `"${nameField}":${valueField}`;
    })
    .reduce((previous, current, index) => (index == 0) ? current : previous + ',' + current, {})
);