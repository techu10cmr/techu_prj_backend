/******************  Carga módulos propios ******************/
const net = require('../utils/net'); // Componer urls
const debugApi = require('../utils/debuggers').api;


function composeUrlRequestMockaroo(typeData) {

    var url;
    if (!typeData) {
        throw new Error("Argumentos typeData es null o blanco");
    } else {

        url = net.composeUrl(`${process.env.MOCKAROO_API_PROTOCOL}`, `${process.env.MOCKAROO_API_HOST}`, `${process.env.MOCKAROO_API_BASE_URL}/${typeData}`);
        debugApi("Url Mockaroo: " + url)
    }

    return (url);
}

function composeHeadersRequestMockaroo() {

    var headers = {};

    headers[process.env.MOCKAROO_API_HEADER_KEY_NAME] = process.env.MOCKAROO_API_HEADER_KEY_VALUE;

    return (headers);
}

module.exports.composeUrlRequestMockaroo = composeUrlRequestMockaroo;
module.exports.composeHeadersRequestMockaroo = composeHeadersRequestMockaroo;