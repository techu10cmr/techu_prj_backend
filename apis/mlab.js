/******************  Carga módulos propios ******************/
const net = require('../utils/net'); // Componer urls
const collections = require('../utils/collections'); // Componer urls
const debugApi = require('../utils/debuggers').api;


function composeQuerystringMlab(arrayQueryFilters) {

  var querystring = {};


  if (arrayQueryFilters && arrayQueryFilters.length > 0) {

    if (arrayQueryFilters[0].nameField === '_id:$oid') {

      querystring.q = `{ "_id": { "$oid": "${arrayQueryFilters[0].valueField}"}}`;

    } else {
      querystring.q = '{' + collections.joinAndStringifyPairsKeyValueArray(arrayQueryFilters) + '}';
    }
  }

  querystring[process.env.MLAB_API_KEY_NAME] = process.env.MLAB_API_KEY_VALUE;

  debugApi("Querystring Mlab: " + JSON.stringify(querystring));

  return (querystring);
}

function composeUrlCollectionMlab(database, collection) {

  var url;
  if (!database || !collection) {
    throw new Error("Argumentos database o collection son null o blanco");
  } else {

    url = net.composeUrl(`${process.env.MLAB_API_PROTOCOL}`, `${process.env.MLAB_API_HOST}`, `${process.env.MLAB_API_PATH_DATABASE}/${database}/collections/${collection}`);
    debugApi("Url Mlab: " + url)
  }

  return (url);
}


/********** Exporta funciones para uso por terceros **********/
module.exports.composeUrlCollectionMlab = composeUrlCollectionMlab;
module.exports.composeQuerystringMlab = composeQuerystringMlab;