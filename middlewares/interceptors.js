const interceptor = require('express-interceptor');
const moment = require('moment');
const debugMiddleware = require('../utils/debuggers').middleware;




exports.logNewRequest = (req, res, next) => {
    debugMiddleware("\n%o", `------------- New Request ${req.id}  (${moment().format('DD/MM/YYYY hh:mm:ss a')}) -------------`);
    debugMiddleware("%s - %o  %o %o %o", "Request", req.method, req.originalUrl, req.params,  req.body);
    next();
};


exports.logResponse = interceptor(function (req, res) {
    return {
        isInterceptable: function () {
            return true;
        },
        intercept: function (body, send) {
            debugMiddleware("%s %o - %o %s", "Response received for request ", req.id, res.statusCode, body);
            debugMiddleware("%o", `------------- End Request ${req.id}  (${moment().format('DD/MM/YYYY hh:mm:ss a')}) -------------`);
            send(body);
        }
    };
});