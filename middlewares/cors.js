const debugMiddleware = require('../utils/debuggers').middleware;


/***** Permitir paso de las peticiones REST (bypass CORS) ****/
exports.enableClientCORS = (req, res, next) => {
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Headers", "Content-Type,iduser,authorization");
    res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
    debugMiddleware("Setted Headers for CORS");
    next();
};