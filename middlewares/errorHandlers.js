const debugMiddleware = require('../utils/debuggers').middleware;
const net = require("../utils/net");


function validationRequestFailed(err, req, res, next) {
  debugMiddleware("Executing validationRequestFailed");

  const body = {
    "msg": `Validation failed for request ${req.id} `,
    "errors": err.message.split(", `")
  };

  net.sendValidationErrorResponse(req, res, body, 400);
}

/********** Exporta para uso por terceros **********/
module.exports.validationRequestFailed = validationRequestFailed;