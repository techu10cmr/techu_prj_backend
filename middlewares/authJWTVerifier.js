var jwt = require('jsonwebtoken');

const debugMiddleware = require('../utils/debuggers').middleware;

function isValidJWTToken(req, res, next) {

    var result;
    debugMiddleware(req.headers);
    debugMiddleware(req.headers.authorization);
    debugMiddleware(req.headers.iduser);

    if (req.headers.authorization && req.headers.iduser) {
        try {
const token  = req.headers.authorization.split(' ')[1];
const user  = req.headers.iduser;
            result = jwt.verify(token);
            debugMiddleware(result);
            console.log(result);
            if (!result) {
                throw new Error("");
            }

        } catch (error) {
            let msg = {
                "msg": "justbankit-login-error",
                "errors": (error.name == 'TokenExpiredError' ?
                    "Expired token" : "Invalid token")
            };
            return res.status(403).send(msg);
        }


        next();
    } else {

        let msg = {
            "msg": "justbankit-login-error",
            "errors": "Incomplete headers for auth"
        }
        return res.status(403).send(msg);

    }
}

module.exports.isValidJWTToken = isValidJWTToken;