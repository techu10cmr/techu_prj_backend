/**************  Carga debuggers propios ***************/
const debugInit = require('./utils/debuggers').init;
const nameApp = 'JustBankIt';
debugInit('App %o booting', nameApp);


/******************  Carga módulos terceros ******************/
const express = require('express');
const dotenv = require('dotenv');
var addRequestId = require('express-request-id')();
debugInit('Loaded third modules');

/******************  Carga middlewares propios *******************/
const corsMw = require('./middlewares/cors');
const interceptorsMw = require('./middlewares/interceptors');
const handleErrorsMw = require('./middlewares/errorHandlers');
debugInit('Loaded own middlewares');

/******************  Carga routers propios *******************/
var authsRouter = require('./routes/auth');
var usersRouter = require('./routes/users');
var accountsRouter = require('./routes/accounts');
debugInit('Loaded own routers');

/********* Configuraciones iniciales  *********/
dotenv.config(); // Carga variables entorno
debugInit('Loaded enviroments variables');

/******************** Arranque servidor **********************/
const app = express();
const port = process.env.SERVER_PORT || 3000;
app.listen(port);
debugInit('App %o booted and listening on port %d', nameApp, port);



/************* Ejecucion de todas las peticiones ****************/
// Middlewares genéricos
app.use(express.json()); // Parsea solo en formato JSON
app.use(addRequestId); // Añade un UUID único a la petición
app.use(interceptorsMw.logNewRequest); // Registrar en modo debug todas las requests
app.use(corsMw.enableClientCORS); // Permite bypass CORS
app.use(interceptorsMw.logResponse); // Registrar en modo debug todas las responses



// Enrutado de peticiones 
app.use('/justbankit/api/v1/auth', authsRouter);
app.use('/justbankit/api/v1/users', usersRouter);
app.use('/justbankit/api/v1/accounts', accountsRouter);

// app.use('^\/justbankit\/api\/v1(?:\/users\/\:oidUser)?\/accounts', accountsRouter);
// app.use('/justbankit/api/v1/accounts/:internalAccountId/movements', accountsRouter);



// Gestión de errores
app.use(handleErrorsMw.validationRequestFailed);
