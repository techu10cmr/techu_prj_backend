/******************  Carga módulos terceros ******************/
const express = require('express');
var usersRouter = express.Router();

/******************  Carga módulos propios ******************/
const usersValidator = require('../validators/UsersValidator');
const usersController = require('../controllers/UserController');
const accountsValidator = require('../validators/AccountsValidator');
const accountsController = require('../controllers/AccountsController');
const authJWTVerifier = require('../middlewares/authJWTVerifier');




/********************* Enrutado de peticiones del recurso login **********************/
usersRouter.get('/', usersController.executeGetUsersV1);
usersRouter.get('/:oidUser', usersValidator.validateGetUserByInternalIdV1, usersController.executeGetUserByInternalIdV1);
usersRouter.post('/', usersValidator.validateCreateUserV1, usersController.executeCreateUserV1);

usersRouter.get('/:oidUser/accounts', accountsValidator.validateGetAccountsByUserV1, accountsController.executeGetAccountsByUserV1);
usersRouter.post('/:oidUser/accounts', accountsValidator.validateCreateAccountV1, accountsController.executeCreateAccountV1);





/********** Exporta para uso por terceros **********/
module.exports = usersRouter;