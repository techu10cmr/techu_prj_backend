/******************  Carga módulos terceros ******************/
const express = require('express');
// var accountsRouter = express.Router({ mergeParams: true });
var accountsRouter = express.Router();

/******************  Carga módulos propios ******************/
const accountsValidator = require('../validators/AccountsValidator');
const accountsController = require('../controllers/AccountsController');


/********************* Enrutado de peticiones del recurso login **********************/


accountsRouter.get('/:oidAccount', accountsValidator.validateGetAccountByInternalIdV1, accountsController.executeGetAccountByInternalIdV1);

accountsRouter.get('/:oidAccount/movements', accountsValidator.validateGetAccountByInternalIdV1, accountsController.executeGetMovemenentsByAccountV1);

accountsRouter.post('/:oidAccount/movements', accountsValidator.validateCreateMovementV1, accountsController.executeCreateMovementV1);





/********** Exporta para uso por terceros **********/
module.exports = accountsRouter;