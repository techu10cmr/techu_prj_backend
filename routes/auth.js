/******************  Carga módulos terceros ******************/
const express = require('express');
var authsRouter = express.Router();


/******************  Carga módulos propios ******************/
const authValidator = require('../validators/AuthValidator'); 
const authController = require('../controllers/AuthController');


/********************* Enrutado de peticiones del recurso login **********************/
authsRouter.post('/login', authValidator.validateLoginUserV1, authController.executeLoginUserV1);
authsRouter.post('/logout/:oidUser', authValidator.validateLogoutUserV1, authController.executeLogoutUserV1);



/********** Exporta funciones para uso por terceros **********/
module.exports = authsRouter;