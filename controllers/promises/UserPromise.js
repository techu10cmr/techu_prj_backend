/******************  Carga módulos terceros ******************/
var sendRequestWithPromises = require('request-promise-native');

/******************  Carga módulos propios ******************/
const promises = require('../../utils/promises');
const mlab = require("../../apis/mlab"); // Gestionar MLAB API
const net = require("../../utils/net"); // Gestionar requests
const crypt = require('../../utils/crypt'); // Gestionar encriptación


function searchUsersByField(nameField, valueField) {

    var queryFiltersUser = (nameField && valueField) ? [{
        'nameField': nameField,
        'valueField': valueField
    }] : [];
    var contentBody = null;
    var request = net.composeBasicRequest('json', 'GET', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_USERS), mlab.composeQuerystringMlab(queryFiltersUser), contentBody);

    return (sendRequestWithPromises(request));
}

function searchAllUsers() {
    return (searchUsersByField(null, null));
}


function oneUserFound(body) {
    return (promises.createPromise(body.length == 1, () => body[0], () => (null)));
}

function noUserFound(body) {
    return (promises.createPromise(body.length == 0, () => (null), () => (null)));
}


function createUser(contentBody) {

    contentBody.login.password = crypt.hash(contentBody.login.password);
    contentBody.login.id = contentBody.login.id.toUpperCase();

    
    var transform2xx = function (body, response) {
        var objOkResponse = {
            "msg": "Usuario creado",
            "oidUser": response.body._id.$oid
        };
        return (objOkResponse);
    };

    var queryFilters = [];

    var request = net.composeBasicRequest('json', 'POST', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_USERS), mlab.composeQuerystringMlab(queryFilters), contentBody, transform2xx, true);

    return (sendRequestWithPromises(request));
}


/********** Exporta funciones para uso por terceros **********/
module.exports.searchUsersByField = searchUsersByField;
module.exports.searchAllUsers = searchAllUsers;
module.exports.oneUserFound = oneUserFound;
module.exports.noUserFound = noUserFound;
module.exports.createUser = createUser;