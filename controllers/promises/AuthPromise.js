/******************  Carga módulos terceros ******************/
var sendRequestWithPromises = require('request-promise-native');



/******************  Carga módulos propios ******************/
const promises = require('../../utils/promises');
const mlab = require("../../apis/mlab"); // Gestionar MLAB API
const net = require("../../utils/net"); // Gestionar requests
const crypt = require('../../utils/crypt'); // Gestionar encriptación
const jwt = require('../../utils/jwt'); // Gestionar encriptación

function loginUser(user) {

    var transform2xx = function () {
        return (user);
    };

    var queryFiltersUserByOID = [{
        'nameField': '_id:$oid',
        'valueField': user._id.$oid
    }];
    var contentBody = {
        "$set": {
            "login.logged": true
        }
    };

    var request = net.composeBasicRequest('json', 'PUT', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_USERS), mlab.composeQuerystringMlab(queryFiltersUserByOID), contentBody, transform2xx, true);

    return (sendRequestWithPromises(request));
}

function processJWTToken(user) {

    var jwtToken;
    try {
        jwtToken = jwt.createToken(user);

    } catch (error) {
        return (false);
    }

    return (jwtToken);
}


function jwtTokenProcessed(user) {

    return (promises.createPromise(processJWTToken(user), jwtToken => ({
        "msg": `${process.env.MSG_RES_LOGIN_OK}`,
        "token": `${jwtToken}`,
        "user": user
    }), () => (user._id.$oid)));
}


function userLogged(user) {
    return (promises.createPromise(user.login.logged, () => user._id.$oid, () => (null)));
}

function logoutUser(oid) {

    var transform2xx = function () {
        var objOkResponse = {
            "msg": `${process.env.MSG_RES_LOGOUT_OK}`,
            "oidUser": oid
        };
        return (objOkResponse);
    };

    var queryFiltersUserByOID = [{
        'nameField': '_id:$oid',
        'valueField': oid
    }];
    var contentBody = {
        "$unset": {
            "login.logged": ""
        }
    };

    var request = net.composeBasicRequest('json', 'PUT', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_USERS), mlab.composeQuerystringMlab(queryFiltersUserByOID), contentBody, transform2xx, true);

    return (sendRequestWithPromises(request));
}

function passwordUserOK(passwordSent, user) {
    return (promises.createPromise(crypt.compare(passwordSent, user.login.password), () => user, () => (null)));
}



/********** Exporta funciones para uso por terceros **********/
module.exports.loginUser = loginUser;
module.exports.logoutUser = logoutUser;
module.exports.userLogged = userLogged;
module.exports.passwordUserOK = passwordUserOK;
module.exports.jwtTokenProcessed = jwtTokenProcessed;