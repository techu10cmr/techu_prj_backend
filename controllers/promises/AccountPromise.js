/******************  Carga módulos terceros ******************/
var sendRequestWithPromises = require('request-promise-native');

/******************  Carga módulos propios ******************/
const mockaroo = require("../../apis/mockaroo"); // Gestionar MLAB API
const net = require("../../utils/net"); // Gestionar requests
const IBAN = require("../../utils/iban");
const mlab = require("../../apis/mlab"); // Gestionar MLAB API

function newMockIbanAccount() {

    var request = net.composeBasicRequest('json', 'POST', mockaroo.composeHeadersRequestMockaroo(), mockaroo.composeUrlRequestMockaroo(process.env.MOCKAROO_API_TYPE_DATA), null, null);

    return (sendRequestWithPromises(request));
}

function createAccount(contentBody, randomNewAccount, internalUserIdSent) {

    contentBody.iban = IBAN.calcular(randomNewAccount, `${process.env.IBAN_COUNTRY}`);
    contentBody.oidUser = internalUserIdSent;
    contentBody.balance = 0;



    var transform2xx = function (body, response) {
        var objOkResponse = {
            "msg": "Cuenta creada",
            "oidAccount": response.body._id.$oid
        };
        return (objOkResponse);
    };

    var queryFilters = [];

    var request = net.composeBasicRequest('json', 'POST', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_ACCOUNTS), mlab.composeQuerystringMlab(queryFilters), contentBody, transform2xx, true);

    return (sendRequestWithPromises(request));
}


function updateAccount(oidAccount, arrayObjsFieldsToUpdate, movement) {


    var transform2xx = function (body, response) {
        var objOkResponse;
        if (!movement) {
            objOkResponse = {
                "msg": "Cuenta actualizada",
                "oidAccount": oidAccount
            };
        } else {
            objOkResponse = {
                "msg": movement.msg,
                "oidMovement": movement.oidMovement
            };
        }
        return (objOkResponse);
    };
    var queryFilters = [{
        'nameField': '_id:$oid',
        'valueField': oidAccount
    }];


    var objFieldsToSet = arrayObjsFieldsToUpdate.filter(obj => !obj.balance).reduce((result, current) => Object.assign(result, current), {});
    var objFieldsToIncrement = arrayObjsFieldsToUpdate.filter(obj => obj.balance).reduce((result, current) => Object.assign(result, current), {});

    var contentBody = {};
    if (!(Object.keys(objFieldsToSet).length === 0 && objFieldsToSet.constructor === Object)) {
        contentBody.$set = objFieldsToSet;
    }

    if (!(Object.keys(objFieldsToIncrement).length === 0 && objFieldsToIncrement.constructor === Object)) {
        contentBody.$inc = objFieldsToIncrement;
    }


    var request = net.composeBasicRequest('json', 'PUT', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_ACCOUNTS), mlab.composeQuerystringMlab(queryFilters), contentBody, transform2xx, true);

    return (sendRequestWithPromises(request));
}


function createMovement(contentBody, internalAccountIdSent) {

    contentBody.oidAccount = internalAccountIdSent;

    var transform2xx = function (body, response) {
        var objOkResponse = {
            "msg": "Movimiento creado",
            "oidMovement": response.body._id.$oid,
            "idTypeMovement": response.body.idType,
            "amountMovement": response.body.amount,

        };
        return (objOkResponse);
    };

    var queryFilters = [];

    var request = net.composeBasicRequest('json', 'POST', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_MOVEMENTS), mlab.composeQuerystringMlab(queryFilters), contentBody, transform2xx, true);

    return (sendRequestWithPromises(request));
}

function searchAccountsByField(nameField, valueField) {

    var queryFiltersAccounts = (nameField && valueField) ? [{
        'nameField': nameField,
        'valueField': valueField
    }] : [];

    var contentBody = null;
    var request = net.composeBasicRequest('json', 'GET', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_ACCOUNTS), mlab.composeQuerystringMlab(queryFiltersAccounts), contentBody);

    return (sendRequestWithPromises(request));
}


function searchMovementsByField(nameField, valueField) {

    var queryFiltersAccounts = (nameField && valueField) ? [{
        'nameField': nameField,
        'valueField': valueField
    }] : [];

    var contentBody = null;
    var request = net.composeBasicRequest('json', 'GET', null, mlab.composeUrlCollectionMlab(process.env.MLAB_DATABASE, process.env.MLAB_API_PATH_COLLECTION_MOVEMENTS), mlab.composeQuerystringMlab(queryFiltersAccounts), contentBody);

    return (sendRequestWithPromises(request));
}


module.exports.newMockIbanAccount = newMockIbanAccount;
module.exports.createAccount = createAccount;
module.exports.searchAccountsByField = searchAccountsByField;
module.exports.createMovement = createMovement;
module.exports.searchMovementsByField = searchMovementsByField;
module.exports.updateAccount = updateAccount;