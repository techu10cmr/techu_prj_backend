/******************  Carga módulos propios ******************/
const net = require("../utils/net"); // Gestionar requests
const debugController = require('../utils/debuggers').controller;
const userPromises = require('../controllers/promises/UserPromise');
const authPromises = require('../controllers/promises/AuthPromise');


exports.executeLoginUserV1 = function executeLoginUserV1(req, res) {
  debugController("Executing executeLoginUserV1");

  var idUserSent = req.body.id.toUpperCase();
  var passwordUserSent = req.body.password;

  userPromises.searchUsersByField('login.id', idUserSent).then(result => userPromises.oneUserFound(result))
    .then(user => authPromises.passwordUserOK(passwordUserSent, user), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGIN_ERROR}`, "Usuario no encontrado")))
    .then(user => authPromises.loginUser(user), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGIN_ERROR}`, "Password no válido")))
    .then(userLoggedOK => authPromises.jwtTokenProcessed(userLoggedOK), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGIN_ERROR}`, "Error actualización usuario")))
    .then(resultOK => net.sendOKResponse(req, res, resultOK, 200), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGIN_ERROR}`, "Error procesamiento token JWT")))
    .catch(error => net.sendErrorResponse(req, res, error));
};


exports.executeLogoutUserV1 = function executeLogoutUserV1(req, res) {
  debugController("Executing executeLogoutUserV1");

  var internalIdUserSent = req.params.oidUser;

  userPromises.searchUsersByField('_id:$oid', internalIdUserSent).then(result => userPromises.oneUserFound(result))
    .then(user => authPromises.userLogged(user), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGOUT_ERROR}`, "Usuario no encontrado")))
    .then((oid) => authPromises.logoutUser(oid), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGOUT_ERROR}`, "Usuario no logado")))
    .then(resultOK => net.sendOKResponse(req, res, resultOK, 200), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, `${process.env.MSG_RES_LOGOUT_ERROR}`, "Error interno en logout")))
    .catch(error => net.sendErrorResponse(req, res, error));
};
