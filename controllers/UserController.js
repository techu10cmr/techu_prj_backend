/******************  Carga módulos propios ******************/
const net = require("../utils/net"); // Gestionar requests
const debugController = require('../utils/debuggers').controller;
const userPromises = require('../controllers/promises/UserPromise');


exports.executeGetUsersV1 = function executeGetUsersV1(req, res) {
  debugController("Executing executeGetUsersV1");

  userPromises.searchAllUsers()
    .then(result => net.sendOKResponse(req, res, result, 200))
    .catch(err => net.sendErrorResponse(req, res, err));
};


exports.executeGetUserByInternalIdV1 = function executeGetUserByInternalIdV1(req, res) {
  debugController("Executing executeGetUserByInternalIdV1");

  var internalIdUserSent = req.params.oidUser;

  userPromises.searchUsersByField('_id:$oid', internalIdUserSent).then(result => userPromises.oneUserFound(result))
    .then(user => net.sendOKResponse(req, res, user, 200), () => net.sendErrorResponse(req, res, net.composeErrorResponse(404, "Busqueda incorrecta", "Usuario no encontrado")))
    .catch(error => net.sendErrorResponse(req, res, error));
};


exports.executeCreateUserV1 = function executeCreateUserV1(req, res) {
  debugController("Executing executeCreateUserV1");

  userPromises.searchUsersByField('login.id', req.body.login.id.toUpperCase()).then(result => userPromises.noUserFound(result))
    .then(() => userPromises.createUser(req.body), () => net.sendErrorResponse(req, res, net.composeErrorResponse(401, "Creación usuario incorrecta", "Ya existe usuario para ese id")))
    .then(result => net.sendOKResponse(req, res, result, 201))
    .catch(err => net.sendErrorResponse(req, res, err));
}