/******************  Carga módulos propios ******************/
const net = require("../utils/net"); // Gestionar requests
const debugController = require('../utils/debuggers').controller;
const accountPromises = require('../controllers/promises/AccountPromise');





exports.executeGetAccountsByUserV1 = function executeGetAccountsByUserV1(req, res) {
  debugController("Executing executeGetAccountsByUserV1");

  accountPromises.searchAccountsByField('oidUser', req.params.oidUser)
    .then(result => net.sendOKResponse(req, res, result, 200))
    .catch(err => net.sendErrorResponse(req, res, err));
}

exports.executeGetAccountByInternalIdV1 = function executeGetAccountByInternalIdV1(req, res) {
  debugController("Executing executeGetAccountByInternalIdV1");

  accountPromises.searchAccountsByField('_id:$oid', req.params.oidAccount)
    .then(result => net.sendOKResponse(req, res, result, 200))
    .catch(err => net.sendErrorResponse(req, res, err));
}

exports.executeGetMovemenentsByAccountV1 = function executeGetMovemenentsByAccountV1(req, res) {
  debugController("Executing executeGetMovemenentsByAccountV1");

  accountPromises.searchMovementsByField('oidAccount', req.params.oidAccount)
    .then(result => net.sendOKResponse(req, res, result, 200))
    .catch(err => net.sendErrorResponse(req, res, err));
}


exports.executeCreateAccountV1 = function executeCreateAccountV1(req, res) {
  debugController("Executing executeCreateAccountV1");

  accountPromises.newMockIbanAccount()
    .then(mock => accountPromises.createAccount(req.body, mock.randomNewAccount, req.params.oidUser), () => net.sendErrorResponse(req, res, net.composeErrorResponse(400, "Creación cuenta incorrecta", "Error en obtener mock IBAN en mockaroo")))
    .then(result => net.sendOKResponse(req, res, result, 201))
    .catch(error => net.sendErrorResponse(req, res, error));
}

exports.executeCreateMovementV1 = function executeCreateMovementV1(req, res) {
  debugController("Executing executeCreateMovementV1");

  var oidAccount = req.params.oidAccount;

  accountPromises.createMovement(req.body, oidAccount)
    .then(movement => accountPromises.updateAccount(oidAccount, [{"balance": movement.amountMovement}], movement), () => net.sendErrorResponse(req, res, net.composeErrorResponse(400, "Creación movimiento incorrecto", "Error en creación movimiento")))
    .then(movement => net.sendOKResponse(req, res, movement, 201))
    .catch(error => net.sendErrorResponse(req, res, error));
}
  