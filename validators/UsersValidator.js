
const validators = require('../utils/validators');


const pathSchemaCreateUserV1 = `${__dirname}/inputSchemas/createUserV1.json`;
var userRequestValidator = validators.getRequestValidator();
userRequestValidator.ajv.addKeyword('dniTypeChecker', {
    modifying: true,
    schema: false,
    errors:true,
    validate: function (data) {
        return (validators.validateDNI(data));
    }
});
exports.validateCreateUserV1 = validators.compileValidatorRequestJSONSchema(userRequestValidator,pathSchemaCreateUserV1);

const pathSchemaGetUserByInternalIdV1 = `${__dirname}/inputSchemas/getUserByInternalIdV1.json`;
exports.validateGetUserByInternalIdV1 = validators.compileValidatorRequestJSONSchema(validators.getRequestValidator(),pathSchemaGetUserByInternalIdV1);


