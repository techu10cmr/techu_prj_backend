const validators = require('../utils/validators');


const pathSchemaLoginUserV1 = `${__dirname}/inputSchemas/loginUserV1.json`;
exports.validateLoginUserV1 = validators.compileValidatorRequestJSONSchema(validators.getRequestValidator(),pathSchemaLoginUserV1);

const pathSchemaLogoutUserV1 = `${__dirname}/inputSchemas/logoutUserV1.json`;
exports.validateLogoutUserV1 = validators.compileValidatorRequestJSONSchema(validators.getRequestValidator(),pathSchemaLogoutUserV1);
