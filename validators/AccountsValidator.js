const validators = require('../utils/validators');


function validateIdType(data) {

    const idsTypeAccount = ["ahorro","credito","personal"];
    return (idsTypeAccount.indexOf(data) > -1);
}

function validateIdBadge(data) {

    const idsTypeAccount = ["EUR","USD","JPY"];
    return (idsTypeAccount.indexOf(data) > -1);
}


const pathSchemaCreateAccountV1 = `${__dirname}/inputSchemas/createAccountV1.json`;
var userRequestValidator = validators.getRequestValidator();

userRequestValidator.ajv.addKeyword('idTypeChecker', {
    modifying: true,
    schema: false,
    errors:true,
    validate: function (data) {
        return (validateIdType(data));
    }
});
userRequestValidator.ajv.addKeyword('idBadgeChecker', {
    modifying: true,
    schema: false,
    errors:true,
    validate: function (data) {
        return (validateIdBadge(data));
    }
});

exports.validateCreateAccountV1 = validators.compileValidatorRequestJSONSchema(userRequestValidator, pathSchemaCreateAccountV1);

const pathSchemaGetAccountsByUserV1 = `${__dirname}/inputSchemas/getAccountsByUserV1.json`;
exports.validateGetAccountsByUserV1 = validators.compileValidatorRequestJSONSchema(validators.getRequestValidator(), pathSchemaGetAccountsByUserV1);

const pathSchemaGetAccountByInternalV1 = `${__dirname}/inputSchemas/getAccountByInternalIdV1.json`;
exports.validateGetAccountByInternalIdV1 = validators.compileValidatorRequestJSONSchema(validators.getRequestValidator(), pathSchemaGetAccountByInternalV1);

const pathSchemaCreateMovementV1 = `${__dirname}/inputSchemas/createMovementV1.json`;
exports.validateCreateMovementV1 = validators.compileValidatorRequestJSONSchema(validators.getRequestValidator(), pathSchemaCreateMovementV1);


